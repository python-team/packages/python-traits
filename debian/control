Source: python-traits
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Varun Hiremath <varun@debian.org>
Build-Depends: debhelper-compat (= 13),
		dh-python,
		pybuild-plugin-pyproject,
		python3-all,
		python3-all-dev,
		python3-setuptools,
		xvfb <!nocheck>,
		xauth <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/enthought/traits
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/python-traits.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-traits

Package: python3-traits
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: Manifest typing and reactive programming for Python (Python 3)
 The traits package provides a metaclass with special attributes that
 are called traits. A trait is a type definition that can be used for
 normal Python object attributes, giving the attributes some
 additional characteristics:
  * Initialization: A trait attribute can have a default value
  * Validation: A trait attribute is manifestly typed.
  * Delegation: The value of a trait attribute can be contained in another
    object
  * Notification: Setting the value of a trait attribute can fired
    callbacks
  * Visualization: With the TraitsUI package, GUIs can be generated
    automatically from traited objects.
 .
 This is the Python 3 version of the package.
