python-traits (6.4.3-2) unstable; urgency=medium

  * Team upload.
  * Fix build failure with Python 3.13 (closes: #1081478).
  * Use pybuild-plugin-pyproject.
  * Update Homepage to https://github.com/enthought/traits.

 -- Colin Watson <cjwatson@debian.org>  Thu, 12 Sep 2024 11:16:25 +0100

python-traits (6.4.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 6.4.3
  * refresh patch
  * add build-dep on xvfb, alike python-traitsui

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 21 Feb 2024 14:04:45 +0100

python-traits (6.3.2-1) unstable; urgency=low

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.
    Closes: #671802

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andreas Tille ]
  * Standards-Version: 4.6.0 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * watch file standard 4 (routine-update), filenamemangle option
  * traits/version.py is not shipped any more with upstream source
    Closes: #671112
  * d/rules: drop useless get-orig-source target
  * Ignore test which fails for unknown reasons for the moment

 -- Andreas Tille <tille@debian.org>  Tue, 08 Feb 2022 10:39:59 +0100

python-traits (5.2.0-2) unstable; urgency=medium

  * Team upload.
  * Drop python2 support; Closes: #938223

 -- Sandro Tosi <morph@debian.org>  Sun, 15 Dec 2019 17:29:30 -0500

python-traits (5.2.0-1) unstable; urgency=medium

  [ Scott Talbert ]
  * Team Upload.
  * Update to new upstream release 5.2.0
  * d/patches: Remove upstreamed patches
  * d/source/options: Ignore changes to traits/version.py
  * d/control: Add mock to Build-Depends for running tests
  * d/control: Update Standards-Version to 4.4.1
  * d/control: Update debhelper-compat level to 12
  * Remove references to sphinx as it was not being used

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

 -- Scott Talbert <swt@techie.net>  Tue, 03 Dec 2019 23:59:06 -0500

python-traits (4.6.0-1) unstable; urgency=medium

  * New upstream release (Closes: #830377)
  * debian/patches:
    - all removed; merged upstream
    - add disable_test.diff to disable failing unittests

 -- Varun Hiremath <varun@debian.org>  Thu, 24 Nov 2016 01:07:10 -0500

python-traits (4.5.0-1) unstable; urgency=low

  [ Jean-Christophe Jaskula ]
  * New upstream release
  * Building python3 package
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Dropped cdbs in favor of dh-* and pybuild
  * debian/patches/
    - remove_testing_dependencies_class.patch: Remove unused, undocumented
      ImportManager and ImportSpy classes, needed for python23 compatibility
      (from upstream)
    - py3_sort_fix.patch: Fix TraitListObject.sort for Python 3, along with
      tests. (from upstream)
    - invalid_tokens_fix.patch: Fix the trait_documenter machinery. (upstream)

 -- Varun Hiremath <varun@debian.org>  Sun, 16 Aug 2015 20:39:49 -0400

python-traits (4.4.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Varun Hiremath ]
  * New upstream release
  * d/rules: Remove reference to pyshared (Closes: #673638)
  * Bump Standards-Version to 3.9.5

 -- Varun Hiremath <varun@debian.org>  Sat, 15 Mar 2014 23:48:58 -0400

python-traits (4.1.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3

 -- Varun Hiremath <varun@debian.org>  Mon, 23 Apr 2012 15:40:31 -0400

python-traits (4.0.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.2
  * Update debian/watch file

 -- Varun Hiremath <varun@debian.org>  Fri, 08 Jul 2011 17:14:58 -0400

python-traits (3.6.0-2) unstable; urgency=low

  * d/rules: fix pyshared directory path (Closes: #621117)

 -- Varun Hiremath <varun@debian.org>  Wed, 06 Apr 2011 19:14:21 -0400

python-traits (3.6.0-1) unstable; urgency=low

  * New upstream release
  * Convert to dh_python2 (Closes: #617037)

 -- Varun Hiremath <varun@debian.org>  Wed, 06 Apr 2011 00:05:26 -0400

python-traits (3.5.0-1) experimental; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Sun, 17 Oct 2010 21:57:56 -0400

python-traits (3.4.0-2) unstable; urgency=low

  * debian/control:
    - Bump XS-Python-Version: >=2.5 (Closes: #589975)
    - Move python-enthoughtbase from Depends to Recommends
    - Bump Standards-Version to 3.9.1
  * debian/copyright:
    - Remove reference to BSD license file

 -- Varun Hiremath <varun@debian.org>  Fri, 30 Jul 2010 02:21:26 -0400

python-traits (3.4.0-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 03 Jun 2010 01:52:55 -0400

python-traits (3.3.0-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - removed Ondrej from Uploaders: thanks for your work!

  [ Varun Hiremath ]
  * New upstream release
  * Switch to source format 3.0
  * Bump dh compat to 7
  * Bump Standards-Version to 3.8.4
  * Remove transition package python-enthought-traits

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Feb 2010 13:37:07 -0500

python-traits (3.2.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.2

 -- Varun Hiremath <varun@debian.org>  Mon, 20 Jul 2009 19:37:05 -0400

python-traits (3.1.0-1) unstable; urgency=low

  * New usptream release

 -- Varun Hiremath <varun@debian.org>  Thu, 26 Mar 2009 19:30:43 -0400

python-traits (3.0.3-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Varun Hiremath ]
  * New upstream release
  * debian/control: add python-setupdocs to Build-Depends

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Dec 2008 22:31:01 -0500

python-traits (3.0.2-1) experimental; urgency=low

  * New upstream release
  * Rename the package to match upstream naming conventions
  * Update debian/watch and debian/orig-tar.sh files

 -- Varun Hiremath <varun@debian.org>  Sun, 26 Oct 2008 00:51:32 -0400

enthought-traits (2.0.5-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.0

 -- Varun Hiremath <varun@debian.org>  Fri, 11 Jul 2008 20:02:42 +0530

enthought-traits (2.0.4-2) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - added versioned build-dep on python-central (>= 0.6)
  * debian/rules
    - fixed due to new python-central directories format (Closes: #472045)

  [ Varun Hiremath ]
  * Make some more fixes in debian/rules

 -- Varun Hiremath <varun@debian.org>  Sun, 23 Mar 2008 10:50:52 +0530

enthought-traits (2.0.4-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Mon, 10 Mar 2008 12:56:13 +0530

enthought-traits (2.0.3-1) unstable; urgency=low

  * New upstream release
  * Add ${shlibs:Depends}, ${misc:Depends} to Depends

 -- Varun Hiremath <varun@debian.org>  Sat, 01 Mar 2008 21:43:37 +0530

enthought-traits (2.0.1b1-2) unstable; urgency=low

  [ Varun Hiremath ]
  * Initial release (Closes: #459349)
  * Rename the source and binary packages.
  * debian/rules:
    + add clean target
    + install using setup files.
  * Install examples and docs.
  * Add debain/orig-tar.sh script to create orig.tar.gz
  * Update debian/copyright file
  * Add debian/README.Debian-source file

  [ Sandro Tosi ]
  * debian/control
    - removed /svn/ from Vcs-Svn
    - added op=log to Vcs-Browser

 -- Varun Hiremath <varun@debian.org>  Tue, 08 Jan 2008 00:02:31 +0530

traits (2.0.1b1-1) UNRELEASED; urgency=low

  [ Ondrej Certik ]
  * First unnofficial package revision

  [ Sandro Tosi ]
  * debian/watch
    - fixed directory parsing for version number

 -- Sandro Tosi <matrixhasu@gmail.com>  Thu, 22 Nov 2007 00:51:01 +0100
